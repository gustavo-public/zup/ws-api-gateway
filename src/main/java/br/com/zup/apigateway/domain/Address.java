package br.com.zup.apigateway.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    private String number;
    private String street;
    private String neighborhood;
    private String town;
    private String state;
    private String complement;
    private String referencePoint;

}
