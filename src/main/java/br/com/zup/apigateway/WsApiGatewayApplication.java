package br.com.zup.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.ExceptionHandler;

@SpringBootApplication
public class WsApiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(WsApiGatewayApplication.class, args);
    }

    @ExceptionHandler(Exception.class)
    public void teste(Throwable ex){
        System.out.println(ex);
    }
}
