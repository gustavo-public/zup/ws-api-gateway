package br.com.zup.apigateway.resolver.query;

import br.com.zup.apigateway.domain.Client;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class ClientQueryResolver implements GraphQLQueryResolver {

    @Autowired
    private WebClient wsClient;

    public Client findByCpf(String cpf){
        return wsClient
                .get()
                .uri("/cpf/"+cpf)
                .exchange()
                .onErrorResume(err-> Mono.error(err))
                .flatMap(res -> res.bodyToMono(Client.class))
                .block();
    }

    public List<Client> findByName(String name){
        return wsClient
                .get()
                .uri("/name/"+name)
                .exchange()
                .flatMapMany(res ->
                                res.bodyToFlux(Client.class)
                ).collectList()
                .block();
    }

    public List<Client> listClients(){
        return wsClient
                .get()
                .exchange()
                .flatMapMany(res ->res.bodyToFlux(Client.class))
                .collectList().block();
    }


}
