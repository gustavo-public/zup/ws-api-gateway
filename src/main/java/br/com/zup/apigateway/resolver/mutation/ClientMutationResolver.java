package br.com.zup.apigateway.resolver.mutation;

import br.com.zup.apigateway.domain.Client;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class ClientMutationResolver implements GraphQLMutationResolver {

    @Autowired
    private WebClient wsClient;

    public Client addClient(Client client){
        return wsClient.post()
                .syncBody(client)
                .retrieve()
                .onStatus(httpStatus -> !httpStatus.is2xxSuccessful(), res->{
                 return   res.bodyToMono(String.class).flatMap(
                            text -> Mono.error(new RuntimeException(text)));
                }).bodyToMono(Client.class)
                .block();
    }

    public Client editClient(Client client){
        return wsClient.put()
                .syncBody(client)
                .retrieve()
                .onStatus(httpStatus -> !httpStatus.is2xxSuccessful(), res->{
                    return   res.bodyToMono(String.class).flatMap(
                            text -> Mono.error(new RuntimeException(text)));
                }).bodyToMono(Client.class)
                .block();
    }

    public String deleteClient(String cpf){
        return wsClient.delete()
                .uri("/"+ cpf)
                .exchange()
                .flatMap(res-> res.bodyToMono(String.class))
                .block();
    }

}
